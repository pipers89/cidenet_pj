import mongoose from "mongoose";
mongoose.Promise = global.Promise;

import dotenv from "dotenv";

dotenv.config();

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  autoIndex: true,
};
const MONGO_DB =
  process.env.MONGO_DB !== undefined
    ? (process.env.MONGO_DB as string)
    : (process.env.MONGO_BD_CLUSTER as string);

export const conexionDB = async () => {
  try {
    await mongoose.connect(MONGO_DB, options);
    // tslint:disable-next-line: no-console
    console.log("Connection established with the server");
  } catch (error) {
    // tslint:disable-next-line: no-console
    console.log("An error occurred in the connection: " + error);
    process.exit(1); // detiene la aplicación
  }
};
