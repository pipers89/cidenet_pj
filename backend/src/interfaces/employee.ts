import mongoose from "mongoose";

export default interface IEmployee extends mongoose.Document {
  firstName: string;
  secondName: string;
  surname: string;
  secondSurname: string;
  country: any;
  identificationType: any;
  identification: string;
  email?: string;
  area: any;
  admissionDate: Date;
  state: string;
}
