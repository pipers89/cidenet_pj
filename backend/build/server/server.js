"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const conection_1 = require("../conection/conection");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
(0, conection_1.conexionDB)();
// Create a new express app instance
const app = (0, express_1.default)();
const PORT = process.env.PORT || 5000;
// enable cors
app.use((0, cors_1.default)());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
// Listen requests
app.listen(PORT, () => {
    // tslint:disable-next-line: no-console
    console.log("Express server port : " + PORT + " \x1b[32m%s\x1b[0m", " onLine");
});
exports.default = app;
//# sourceMappingURL=server.js.map