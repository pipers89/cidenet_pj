export interface Employee {
  _id?: string;
  firstName?: string;
  secondName?: string;
  surname?: string;
  secondSurname?: string;
  country?: any;
  identificationType?: any;
  identification?: string;
  email?: string;
  area?: any;
  admissionDate?: Date;
  state?: string;
}
