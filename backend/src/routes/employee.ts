import express, { Request, Response } from "express";
import dotenv from "dotenv";

// Utilities
import {
  handleServerError,
  handlerMessageOK,
  handleMessage,
} from "../utils/handlerMessages";

// Imports models
import {
  EmployeeModel,
  AreaModel,
  IdentificationTypeModel,
  CountryModel,
} from "../models";
import IEmployee from "../interfaces/employee";
import { MESSAGE, DOMAIN_CO, DOMAIN_US } from "../utils/constants";

// Set config env
dotenv.config();

const router: express.Router = express.Router();

export interface ObjGenerated {
  email: String;
  isGenerated: Boolean;
}

router.get("/", async (req: Request, res: Response) => {
  const { firstName, secondName, surname, secondSurname, country, ofSet } =
    req.query;
  let oset: number = 0,
    queryResult: any = {};

  if (firstName !== "" && firstName !== undefined) {
    queryResult.firstName = firstName;
  }
  if (secondName !== "" && secondName !== undefined) {
    queryResult.secondName = secondName;
  }
  if (surname !== "" && surname !== undefined) {
    queryResult.surname = surname;
  }
  if (secondSurname !== "" && secondSurname !== undefined) {
    queryResult.secondSurname = secondSurname;
  }
  if (country !== "" && country !== undefined) {
    queryResult.country = country;
  }
  if (ofSet !== "" && ofSet !== undefined) {
    oset = Number(oset);
    queryResult.ofSet = oset || 0;
  }

  return EmployeeModel.find(queryResult)
    .populate("area")
    .populate("country")
    .populate("identificationType")
    .skip(oset)
    .limit(10)
    .exec((err: any, employees: any) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_load, err);
      }

      return EmployeeModel.countDocuments(
        queryResult,
        (_err: any, count: number) => {
          handlerMessageOK(res, employees, count);
        }
      );
    });
});

router.get("/:id", async (req: Request, res: Response) => {
  const idEmployee = req.params.id;
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  return EmployeeModel.findById(idEmployee)
    .populate("area")
    .populate("country")
    .populate("identificationType")
    .skip(ofSet)
    .limit(10)
    .exec((err: any, employee) => {
      if (err) {
        handleServerError(res, MESSAGE.no_found, err);
      }

      if (!employee) {
        handleMessage(
          res,
          `El empleado con ID: ${idEmployee} no esta registrado.`
        );
      }

      EmployeeModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, employee, count);
      });
    });
});

router.post("/post", async (req: Request, res: Response) => {
  const {
    firstName,
    secondName,
    surname,
    secondSurname,
    country,
    identificationType,
    identification,
    email,
    password,
    area,
    admissionDate,
    state,
  } = req.body;

  const newEmployee = new EmployeeModel({
    firstName,
    secondName,
    surname,
    secondSurname,
    country,
    identificationType,
    identification,
    email,
    password,
    area,
    admissionDate,
    state,
  });

  const areaFound = await AreaModel.findById(area);
  const countryFound = await CountryModel.findById(country);
  const ITFound = await IdentificationTypeModel.findById(identificationType);

  if (!areaFound) {
    newEmployee.area = areaFound;
  }

  if (!countryFound) {
    newEmployee.country = countryFound;
  }

  if (!ITFound) {
    newEmployee.identificationType = ITFound;
  }

  // Create email by data employee
  const generateEmail = await GenerateUniqueEmail(newEmployee);
  newEmployee.email = generateEmail;

  newEmployee.save((err: any, employeeSave: IEmployee) => {
    if (err) {
      handleServerError(res, MESSAGE.failed_save, err);
    }

    handlerMessageOK(res, employeeSave);
  });
});

router.put("/:id", async (req: Request, res: Response) => {
  const idEmployee = req.params.id;

  // Verify employee
  const updateEmployee = await EmployeeModel.findById(idEmployee);

  if (!updateEmployee) {
    handleMessage(
      res,
      `El empleado con ID: ${idEmployee} no se encuentra registrado.`
    );
  }

  return await EmployeeModel.findOneAndUpdate(
    { _id: idEmployee },
    req.body,
    { new: true },
    (err: any, employee) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_update, err);
      }

      if (!employee) {
        handleMessage(res, `No existe un registro con el ID: ${idEmployee}`);
      }

      handlerMessageOK(res, employee);
    }
  );
});

router.delete("/:id", async (req: Request, res: Response) => {
  const idEmployee = req.params.id;

  await EmployeeModel.findByIdAndRemove(
    idEmployee,
    (err: any, employeeDelete: any) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_delete, err);
      }

      if (!employeeDelete) {
        handleMessage(res, `No existe un registro con el ID: ${idEmployee}`);
      }

      handlerMessageOK(res, employeeDelete);
    }
  );
});

async function GenerateUniqueEmail(user: IEmployee): Promise<any> {
  const countryFound = await CountryModel.findById(user.country);
  const identifyUser: string =
    countryFound?.shortName === "co" ? DOMAIN_CO : DOMAIN_US;
  const emailGenerated = `${user.firstName.toLocaleLowerCase()}.${user.surname.toLocaleLowerCase()}@${identifyUser}`;

  return emailGenerated;
}

export default router;
