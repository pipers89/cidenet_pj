import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import Swal from 'sweetalert2';
import { Employee } from '../interfaces/employee.interface';

@Component({
  selector: 'app-create-update-employee',
  templateUrl: './create-update-employee.component.html',
  styleUrls: ['./create-update-employee.component.css'],
})
export class CreateUpdateEmployeeComponent implements OnInit {
  @Input() public labelNameModal!: string;
  @Input() public idEmployee!: string;
  @Output() createOrUpdateEmployee = new EventEmitter<boolean>();

  public areas: any[] = [];
  public countries: any[] = [];
  public typeIdentifications: any[] = [];
  public employeeForm: FormGroup = this.FB.group({
    _id: [undefined],
    firstName: [undefined, [Validators.required, Validators.max(20)]],
    secondName: [undefined, Validators.max(50)],
    surname: [undefined, [Validators.required, Validators.max(20)]],
    secondSurname: [undefined, [Validators.required, Validators.max(20)]],
    country: [undefined],
    identificationType: [undefined],
    identification: [undefined, [Validators.maxLength(20)]],
    email: [undefined],
    area: [undefined],
    admissionDate: [undefined],
    state: [{ value: true, disabled: true }],
  });

  constructor(
    private employeeService: EmployeeService,
    private FB: FormBuilder
  ) {}

  public async ngOnInit() {
    if (this.idEmployee !== '' && this.idEmployee !== undefined) {
      const resEmployeeById = await this.employeeService.getEmployeeById(
        this.idEmployee
      );

      if (resEmployeeById !== null) {
        let employee: Employee = resEmployeeById.result;
        this.employeeForm.patchValue(employee);
      }
    }
    await this.getAreas();
    await this.getCountries();
    await this.getTypeIdentifications();
  }

  public async validateAction() {
    if (this.employeeForm.valid) {
      let model = this.objDeepCopy(this.employeeForm.value);
      if (this.labelNameModal === 'Crear') {
        const resSave = await this.employeeService.saveEmployee(model);
        if (resSave !== null) {
          Swal.fire(
            'Registro!',
            'Empleado registrado correctamente.',
            'success'
          );
        }
      } else {
        const resUpdate = await this.employeeService.updateEmployee(
          model._id,
          model
        );
        if (resUpdate !== null) {
          Swal.fire(
            'Registro!',
            'Empleado actualizado correctamente.',
            'success'
          );
        }
      }

      this.employeeForm.reset();
      this.createOrUpdateEmployee.emit(true);
    }
  }

  public async updateEmployee() {}

  public async getAreas() {
    const resAreas = await this.employeeService.getAllAreas();

    if (resAreas !== null && resAreas.ok) {
      this.areas = resAreas.result;
    }
  }

  public async getTypeIdentifications() {
    const resTypeIdentifications =
      await this.employeeService.getAllTypeIdentifications();

    if (resTypeIdentifications !== null && resTypeIdentifications.ok) {
      this.typeIdentifications = resTypeIdentifications.result;
    }
  }

  public async getCountries() {
    const resCountries = await this.employeeService.getAllCountries();

    if (resCountries !== null && resCountries.ok) {
      this.countries = resCountries.result;
    }
  }

  public objDeepCopy(obj: any) {
    let copy: any;

    // Handle the 3 simple types, and null or undefined
    // tslint:disable-next-line:no-null-keyword
    if (null === obj || 'object' !== typeof obj) {
      return obj;
    }

    // Handle Date
    if (obj instanceof Date) {
      copy = new Date();
      copy.setTime(obj.getTime());

      return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
      copy = [];
      for (let i = 0, len = obj.length; i < len; i++) {
        copy[i] = this.objDeepCopy(obj[i]);
      }

      return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
      copy = {};
      for (const attr in obj) {
        if (obj.hasOwnProperty(attr)) {
          copy[attr] = this.objDeepCopy(obj[attr]);
        }
      }

      return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
  }
}
