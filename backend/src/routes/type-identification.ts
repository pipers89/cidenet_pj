import express, { Request, Response } from "express";
import dotenv from "dotenv";

// Utilities
import {
  handleServerError,
  handlerMessageOK,
  handleMessage,
} from "../utils/handlerMessages";
import { MESSAGE } from "../utils/constants";
import { IdentificationTypeModel } from "../models";
import ITypeIdentification from "../interfaces/type-identification";

// Set config env
dotenv.config();

const router: express.Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  return IdentificationTypeModel.find({})
    .skip(ofSet)
    .limit(10)
    .exec((err: any, areas: ITypeIdentification[]) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_load, err);
      }

      return IdentificationTypeModel.countDocuments(
        {},
        (_err: any, count: number) => {
          handlerMessageOK(res, areas, count);
        }
      );
    });
});
router.get("/:id", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  const idTypeIdentification = req.params.id;

  return IdentificationTypeModel.findById(idTypeIdentification)
    .skip(ofSet)
    .limit(10)
    .exec((err: any, typeIdentification: ITypeIdentification) => {
      if (err) {
        handleServerError(res, MESSAGE.no_found, err);
      }

      if (!typeIdentification) {
        handleMessage(
          res,
          `El empleado con ID: ${idTypeIdentification} no esta registrado.`
        );
      }

      IdentificationTypeModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, typeIdentification, count);
      });
    });
});
router.post("/post", async (req: Request, res: Response) => {
  const { nameIdentification } = req.body;
  const saveTypeIdentification = new IdentificationTypeModel({
    nameIdentification,
  });

  saveTypeIdentification.save((err: any, areaSave: ITypeIdentification) => {
    if (err) {
      handleServerError(res, MESSAGE.failed_save, err);
    }

    handlerMessageOK(res, areaSave);
  });
});
router.put("/:id", async (req: Request, res: Response) => {
  const idTypeIdentification = req.params.id;
  const updateTypeIdentification = await IdentificationTypeModel.findById(
    idTypeIdentification
  );

  if (!updateTypeIdentification) {
    handleMessage(
      res,
      `El area con ID: ${idTypeIdentification} no se encuentra registrado`
    );
  }

  return await IdentificationTypeModel.findOneAndUpdate(
    { _id: idTypeIdentification },
    req.body,
    { new: true },
    (err: any, typeIdentificationUpdate) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_update, err);
      }

      if (!typeIdentificationUpdate) {
        handleMessage(
          res,
          `No existe un registro con el ID: ${idTypeIdentification}`
        );
      }

      handlerMessageOK(res, typeIdentificationUpdate);
    }
  );
});
router.delete("/:id", async (req: Request, res: Response) => {
  const idTypeIdentification = req.params.id;

  await IdentificationTypeModel.findByIdAndRemove(
    idTypeIdentification,
    (err: any, typeIdentificationDelete: any) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_delete, err);
      }

      if (!typeIdentificationDelete) {
        handleMessage(
          res,
          `No existe un registro con el ID: ${typeIdentificationDelete}`
        );
      }

      handlerMessageOK(res, typeIdentificationDelete);
    }
  );
});

export default router;
