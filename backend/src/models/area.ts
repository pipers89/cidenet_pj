import mongoose, { Schema } from "mongoose";
import IArea from "../interfaces/area";

const AreaSchema = new Schema({
  nameArea: {
    type: String,
    required: true,
    trim: true,
  },
  observation: {
    type: String,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

export default mongoose.model<IArea>("Area", AreaSchema);
