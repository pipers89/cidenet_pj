"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.conexionDB = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    autoIndex: true,
};
const MONGO_DB = process.env.MONGO_DB !== undefined
    ? process.env.MONGO_DB
    : process.env.MONGO_BD_CLUSTER;
mongoose_1.default.Promise = global.Promise;
const conexionDB = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield mongoose_1.default.connect(MONGO_DB, options);
        // tslint:disable-next-line: no-console
        console.log("Connection established with the server");
    }
    catch (error) {
        // tslint:disable-next-line: no-console
        console.log("An error occurred in the connection: " + error);
        process.exit(1); // detiene la aplicación
    }
});
exports.conexionDB = conexionDB;
//# sourceMappingURL=conection.js.map