import mongoose from "mongoose";

export default interface IArea extends mongoose.Document {
  nameArea: string;
  observation?: string;
}
