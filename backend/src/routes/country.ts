import express, { Request, Response } from "express";
import dotenv from "dotenv";

// Utilities
import {
  handleServerError,
  handlerMessageOK,
  handleMessage,
} from "../utils/handlerMessages";
import { MESSAGE } from "../utils/constants";
import { CountryModel } from "../models";
import ICountry from "../interfaces/country";

// Set config env
dotenv.config();

const router: express.Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  return CountryModel.find({})
    .skip(ofSet)
    .limit(10)
    .exec((err: any, countries: ICountry[]) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_load, err);
      }

      return CountryModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, countries, count);
      });
    });
});
router.get("/:id", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);
  const idCountry = req.params.id;

  return CountryModel.findById(idCountry)
    .skip(ofSet)
    .limit(10)
    .exec((err: any, area: ICountry) => {
      if (err) {
        handleServerError(res, MESSAGE.no_found, err);
      }

      if (!area) {
        handleMessage(
          res,
          `El empleado con ID: ${idCountry} no esta registrado.`
        );
      }

      CountryModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, area, count);
      });
    });
});
router.post("/post", async (req: Request, res: Response) => {
  const { nameCountry, shortName } = req.body;
  const saveCountry = new CountryModel({
    nameCountry,
    shortName,
  });

  saveCountry.save((err: any, countrySave: ICountry) => {
    if (err) {
      handleServerError(res, MESSAGE.failed_save, err);
    }

    handlerMessageOK(res, countrySave);
  });
});
router.put("/:id", async (req: Request, res: Response) => {
  const idCountry = req.params.id;
  const updateCountry = await CountryModel.findById(idCountry);

  if (!updateCountry) {
    handleMessage(
      res,
      `El area con ID: ${idCountry} no se encuentra registrado`
    );
  }

  return await CountryModel.findOneAndUpdate(
    { _id: idCountry },
    req.body,
    { new: true },
    (err: any, countryUpdate) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_update, err);
      }

      if (!countryUpdate) {
        handleMessage(res, `No existe un registro con el ID: ${idCountry}`);
      }

      handlerMessageOK(res, countryUpdate);
    }
  );
});
router.delete("/:id", async (req: Request, res: Response) => {
  const idCountry = req.params.id;

  await CountryModel.findByIdAndRemove(
    idCountry,
    (err: any, countryDelete: any) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_delete, err);
      }

      if (!countryDelete) {
        handleMessage(res, `No existe un registro con el ID: ${countryDelete}`);
      }

      handlerMessageOK(res, countryDelete);
    }
  );
});

export default router;
