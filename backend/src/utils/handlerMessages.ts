import { Response } from "express";

export function BadRequestMessage(res: Response, message: string) {
  return res.status(401).json({
    message,
    error: message,
  });
}

export function handleMessage(res: Response, message: string) {
  return res.status(400).json({
    ok: false,
    message,
  });
}

export function handleServerError(res: Response, message: string, error: any) {
  return res.status(500).json({
    ok: false,
    message,
    error,
  });
}

export function handlerMessageOK(
  res: Response,
  model: any,
  countRegisters?: number
) {
  return res.status(200).json({
    ok: true,
    result: model,
    total: countRegisters,
  });
}
