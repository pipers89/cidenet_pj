import { Component, OnInit, PipeTransform } from '@angular/core';
import { Employee } from '../interfaces/employee.interface';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { EmployeeService } from '../services/employee.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css'],
})
export class ListEmployeeComponent implements OnInit {
  public employeeList: Employee[] = [];
  public countries$!: Observable<Employee[]>;
  filter = new FormControl('');
  public labelNameModal: string = '';
  public page = 1;
  public countCollections: number = 0;
  public pageSize: number = 5;
  public idEmployee: string = '';

  constructor(
    private employeeService: EmployeeService,
    private modalService: NgbModal
  ) {}

  public async ngOnInit() {
    await this.refresh();
  }

  public async refresh() {
    await this.getAllEmployees();
    this.countries$ = this.filter.valueChanges.pipe(
      startWith(''),
      map((text) => this.searchEmployee(text))
    );
  }

  public searchEmployee(text: string): Employee[] {
    this.refreshEmployeesList();
    return this.employeeList.filter((employee: Employee) => {
      const term = text.toLowerCase();
      return (
        employee.firstName?.toLowerCase().includes(term) ||
        employee.secondName?.toLowerCase().includes(term) ||
        employee.surname?.toLowerCase().includes(term) ||
        employee.secondSurname?.toLowerCase().includes(term) ||
        employee.email?.toLowerCase().includes(term)
      );
      // || pipe.transform(employee.area).includes(term)
      // || pipe.transform(employee.population).includes(term);
    });
  }

  public refreshEmployeesList() {
    this.employeeList = this.employeeList
      .map((e, i) => ({ id: i + 1, ...e }))
      .slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize
      );
  }

  public async openNewOrUpdateEmployee(content: any, employee?: Employee) {
    this.identifyAction(employee);
    this.modalService.open(content, { size: 'lg' });
  }

  public async identifyAction(employee?: Employee) {
    if (employee?._id !== undefined && employee?._id !== '') {
      this.labelNameModal = 'Actualizar';
      this.idEmployee = employee?._id;
    } else {
      this.labelNameModal = 'Crear';
    }
  }

  public async deleteEmployee(idEmployee: string) {
    Swal.fire({
      title: 'Eliminar',
      text: 'Está seguro de que desea eliminar el empleado? ',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(idEmployee).then(() => {
          Swal.fire(
            'Eliminado!',
            'El registro se elimino exitosamente.',
            'success'
          );
          this.refresh();
        });
      }
    });
  }

  public async createOrUpdateSuccess(modal: any) {
    modal.dismiss();
    this.refresh();
  }

  public async getAllEmployees() {
    const resAllEmployee = await this.employeeService.getAllEmployees();

    if (resAllEmployee !== null && resAllEmployee.ok) {
      this.employeeList = resAllEmployee.result;
      this.countCollections = resAllEmployee.total;
    }
  }
}
