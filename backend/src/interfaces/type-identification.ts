import mongoose from "mongoose";

export default interface ITypeIdentification extends mongoose.Document {
  nameIdentification: string;
}
