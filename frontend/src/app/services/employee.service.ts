import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Employee } from '../interfaces/employee.interface';

@Injectable()
export class EmployeeService {
  private URL: string = `${environment.apiUrl}`;

  constructor(private http: HttpClient) {}

  public saveEmployee(employee: Employee): any {
    return this.http
      .post(`${this.URL}employee/post`, employee)
      .toPromise()
      .then((res) => res);
  }

  public updateEmployee(id: string, employee: Employee): any {
    return this.http
      .put(`${this.URL}employee/${id}`, employee)
      .toPromise()
      .then((res) => res);
  }

  public deleteEmployee(id: string): any {
    return this.http
      .delete(`${this.URL}employee/${id}`)
      .toPromise()
      .then((res) => res);
  }

  public async getAllEmployees(
    firstName?: string,
    secondName?: string,
    surname?: string,
    secondSurname?: string,
    country?: string,
    ofset?: number
  ): Promise<any> {
    let URL: string = `${this.URL}employee?`;

    if (firstName !== '' && firstName !== undefined) {
      URL += `offset=${ofset}&`;
    }
    if (secondName !== '' && secondName !== undefined) {
      URL += `secondName=${firstName}&`;
    }
    if (surname !== '' && surname !== undefined) {
      URL += `surname=${firstName}&`;
    }
    if (secondSurname !== '' && secondSurname !== undefined) {
      URL += `secondSurname=${firstName}&`;
    }
    if (country !== '' && country !== undefined) {
      URL += `country=${firstName}&`;
    }
    if (ofset !== undefined && ofset > 0) {
      URL += `ofset=${firstName}&`;
    }

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllAreas(): Promise<any> {
    let URL: string = `${this.URL}areas`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllCountries(): Promise<any> {
    let URL: string = `${this.URL}countries`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllTypeIdentifications(): Promise<any> {
    let URL: string = `${this.URL}typeIdentification`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public getEmployeeById(id: string) {
    let URL: string = `${this.URL}employee/${id}`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }
}
