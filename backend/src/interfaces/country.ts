import mongoose from "mongoose";

export default interface ICountry extends mongoose.Document {
  nameCountry: string;
  shortName: string;
}
