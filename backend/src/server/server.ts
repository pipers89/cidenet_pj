import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { conexionDB } from "../conection/conection";
import dotenv from "dotenv";

import {
  EmployeeRoute,
  AreasRoute,
  CountriesRoute,
  TypeIdentificationRoute,
} from "../routes";

dotenv.config();
conexionDB();

// Create a new express app instance
const app: express.Application = express();
const PORT = process.env.PORT || 5000;

// enable cors
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Registers routes
app.use("/api/employee", EmployeeRoute);
app.use("/api/areas", AreasRoute);
app.use("/api/countries", CountriesRoute);
app.use("/api/typeIdentification", TypeIdentificationRoute);

// Listen requests
app.listen(PORT, () => {
  // tslint:disable-next-line: no-console
  console.log(
    "Express server port : " + PORT + " \x1b[32m%s\x1b[0m",
    " onLine"
  );
});

export default app;
