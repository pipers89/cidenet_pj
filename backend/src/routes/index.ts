export { default as EmployeeRoute } from "./employee";
export { default as AreasRoute } from "./area";
export { default as TypeIdentificationRoute } from "./type-identification";
export { default as CountriesRoute } from "./country";
