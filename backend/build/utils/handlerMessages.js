"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handlerMessageOK = exports.handleServerError = exports.BadRequestMessage = void 0;
function BadRequestMessage(res, message) {
    return res.status(401).json({
        message,
        error: message,
    });
}
exports.BadRequestMessage = BadRequestMessage;
function handleServerError(res, message, error) {
    return res.status(500).json({
        ok: false,
        message,
        error,
    });
}
exports.handleServerError = handleServerError;
function handlerMessageOK(res, model, countRegisters) {
    return res.status(200).json({
        ok: true,
        model,
        total: countRegisters,
    });
}
exports.handlerMessageOK = handlerMessageOK;
//# sourceMappingURL=handlerMessages.js.map