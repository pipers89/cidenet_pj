"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Utilities
const handlerMessages_1 = require("../utils/handlerMessages");
// Imports models
const models_1 = require("../models");
// Set config env
dotenv_1.default.config();
const router = express_1.default.Router();
router.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let ofSet = req.query.ofSet || 0;
    ofSet = Number(ofSet);
    return models_1.employeeModel.find({}).exec((err, employees) => {
        if (err) {
            (0, handlerMessages_1.handleServerError)(res, "Error al cargar la información", err);
        }
        models_1.employeeModel.countDocuments({}, (_err, count) => {
            (0, handlerMessages_1.handlerMessageOK)(res, employees, count);
        });
    });
}));
router.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const body = req.body;
}));
//# sourceMappingURL=employee.js.map