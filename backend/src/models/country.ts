import mongoose, { Schema } from "mongoose";
import ICountry from "../interfaces/country";

const CountrySchema = new Schema({
  nameCountry: {
    type: String,
    required: true,
    trim: true,
  },
  shortName: {
    type: String,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

export default mongoose.model<ICountry>("Country", CountrySchema);
