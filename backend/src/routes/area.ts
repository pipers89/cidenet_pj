import express, { Request, Response } from "express";
import dotenv from "dotenv";

// Utilities
import {
  handleServerError,
  handlerMessageOK,
  handleMessage,
} from "../utils/handlerMessages";

// Import models and utils
import { AreaModel } from "../models";
import { MESSAGE } from "../utils/constants";
import IArea from "../interfaces/area";

// Set config env
dotenv.config();

const router: express.Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  return AreaModel.find({})
    .skip(ofSet)
    .limit(10)
    .exec((err: any, areas: IArea[]) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_load, err);
      }

      return AreaModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, areas, count);
      });
    });
});
router.get("/:id", async (req: Request, res: Response) => {
  let ofSet = req.query.ofSet || 0;
  ofSet = Number(ofSet);

  const idArea = req.params.id;
  return AreaModel.findById(idArea)
    .skip(ofSet)
    .limit(10)
    .exec((err: any, area: IArea) => {
      if (err) {
        handleServerError(res, MESSAGE.no_found, err);
      }

      if (!area) {
        handleMessage(res, `El empleado con ID: ${idArea} no esta registrado.`);
      }

      AreaModel.countDocuments({}, (_err: any, count: number) => {
        handlerMessageOK(res, area, count);
      });
    });
});
router.post("/post", async (req: Request, res: Response) => {
  const { nameArea, observation } = req.body;
  const saveArea = new AreaModel({
    nameArea,
    observation,
  });

  saveArea.save((err: any, areaSave: IArea) => {
    if (err) {
      handleServerError(res, MESSAGE.failed_save, err);
    }

    handlerMessageOK(res, areaSave);
  });
});
router.put("/:id", async (req: Request, res: Response) => {
  const idArea = req.params.id;
  const updateArea = await AreaModel.findById(idArea);

  if (!updateArea) {
    handleMessage(res, `El area con ID: ${idArea} no se encuentra registrado`);
  }

  return await AreaModel.findOneAndUpdate(
    { _id: idArea },
    req.body,
    { new: true },
    (err: any, areaUpdate) => {
      if (err) {
        handleServerError(res, MESSAGE.failed_update, err);
      }

      if (!areaUpdate) {
        handleMessage(res, `No existe un registro con el ID: ${idArea}`);
      }

      handlerMessageOK(res, areaUpdate);
    }
  );
});
router.delete("/:id", async (req: Request, res: Response) => {
  const idArea = req.params.id;

  await AreaModel.findByIdAndRemove(idArea, (err: any, areaDelete: any) => {
    if (err) {
      handleServerError(res, MESSAGE.failed_delete, err);
    }

    if (!areaDelete) {
      handleMessage(res, `No existe un registro con el ID: ${areaDelete}`);
    }

    handlerMessageOK(res, areaDelete);
  });
});

export default router;
