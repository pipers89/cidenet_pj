import dotenv from "dotenv";

// Set config env
dotenv.config();

export const DOMAIN_CO: string = "cidenet.com.co ";
export const DOMAIN_US: string = "cidenet.com.us ";
export const MESSAGE = {
  success_save: "Registro creado exitosamente.",
  success_update: "Registro actualizado exitosamente.",
  success_delete: "Registro eliminado exitosamente.",
  failed_save: "No se pudo crear el registro.",
  failed_update: "No se pudo actualizar el registro.",
  failed_delete: "No se pudo eliminar el registro.",
  failed_load: "Error al cargar los registros",
  no_found: "No se logro encontrar el registro.",
};
