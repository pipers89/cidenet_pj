"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.identificationTypeModel = exports.countryModel = exports.areaModel = exports.employeeModel = void 0;
var employee_1 = require("./employee");
Object.defineProperty(exports, "employeeModel", { enumerable: true, get: function () { return __importDefault(employee_1).default; } });
var area_1 = require("./area");
Object.defineProperty(exports, "areaModel", { enumerable: true, get: function () { return __importDefault(area_1).default; } });
var country_1 = require("./country");
Object.defineProperty(exports, "countryModel", { enumerable: true, get: function () { return __importDefault(country_1).default; } });
var type_identification_1 = require("./type-identification");
Object.defineProperty(exports, "identificationTypeModel", { enumerable: true, get: function () { return __importDefault(type_identification_1).default; } });
//# sourceMappingURL=index.js.map