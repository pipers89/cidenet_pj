import mongoose, { Schema } from "mongoose";
import uniqueValidator from "mongoose-unique-validator";
import IEmployee from "../interfaces/employee";

const EmployeeSchema = new Schema({
  firstName: {
    type: String,
    required: [true, "Pimer nombre requerido"],
    trim: true,
    unique: true,
  },
  secondName: {
    type: String,
    trim: true,
    unique: true,
  },
  surname: {
    type: String,
    required: [true, "Pimer apellido requerido"],
    trim: true,
    unique: true,
  },
  secondSurname: {
    type: String,
    required: [true, "Segundo apellido requerido"],
    trim: true,
    unique: true,
  },
  country: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Country",
  },
  identificationType: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "IdentificationType",
  },
  identification: {
    type: String,
    required: [true, "Número de identificación requerido"],
    trim: true,
    unique: true,
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    uniqueCaseInsensitive: true,
  },
  area: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Area",
  },
  admissionDate: { type: Date, required: [true, "Fecha de ingreso requerida"] },
  state: { type: Boolean, default: true },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

EmployeeSchema.plugin(uniqueValidator, { message: "{PATH} debe ser unico" }); // Validator field schema

export default mongoose.model<IEmployee>("Employee", EmployeeSchema);
