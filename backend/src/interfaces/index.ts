export { default as IEmployee } from "./employee";
export { default as IArea } from "./area";
export { default as ITypeIdentification } from "./type-identification";
export { default as ICountry } from "./country";
