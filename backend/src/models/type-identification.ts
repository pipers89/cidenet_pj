import mongoose, { Schema } from "mongoose";
import ITypeIdentification from "../interfaces/type-identification";

const IdentificationTypeSchema = new Schema({
  nameIdentification: {
    type: String,
    required: true,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

export default mongoose.model<ITypeIdentification>(
  "IdentificationType",
  IdentificationTypeSchema
);
